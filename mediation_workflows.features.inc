<?php
/**
 * @file
 * mediation_workflows.features.inc
 */

/**
 * Implementation of hook_workflow_features_default_workflow().
 */
function mediation_workflows_workflow_features_default_workflow() {
  return array(
    'DITA staging' => array(
      'name' => 'DITA staging',
      'tab_roles' => 'author,4',
      'options' => 'a:3:{s:16:"comment_log_node";i:0;s:15:"comment_log_tab";i:0;s:13:"name_as_title";i:0;}',
      'states' => array(
        0 => array(
          'state' => '(creation)',
          'weight' => '-50',
          'sysid' => '1',
          'status' => '1',
        ),
        1 => array(
          'state' => 'Initial Tagging',
          'weight' => '0',
          'sysid' => '0',
          'status' => '1',
        ),
        2 => array(
          'state' => 'Review Initial Tagging',
          'weight' => '1',
          'sysid' => '0',
          'status' => '1',
        ),
      ),
      'transitions' => array(
        0 => array(
          'sid' => '(creation)',
          'target_sid' => 'Initial Tagging',
          'roles' => 'author,4',
        ),
        1 => array(
          'sid' => '(creation)',
          'target_sid' => 'Review Initial Tagging',
          'roles' => 'author,4',
        ),
        2 => array(
          'sid' => 'Initial Tagging',
          'target_sid' => 'Review Initial Tagging',
          'roles' => 'author,4',
        ),
        3 => array(
          'sid' => 'Review Initial Tagging',
          'target_sid' => 'Initial Tagging',
          'roles' => 'author,4',
        ),
      ),
      'node_types' => array(
        0 => array(
          'type' => 'topic',
          'wid' => '1',
        ),
      ),
    ),
  );
}
